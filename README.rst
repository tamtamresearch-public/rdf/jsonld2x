====================
JSON-LD transformers
====================
This repo provides simple tool for converting JSON-LD into other formats, testing them and publishing converted results as GitLab pages.

Main motivation is to explore possible issues of vocabs http://datex2.eu/vocab/3/ but it may be used for other vocabularies too.

Installation
============
Use python 3.9 and use poetry. Cd to project dir::

    $ poetry install

Activate virtual env
====================
Before further use, you shall activate the python virtual environment::

    $ poetry shell

From now on it is assumed you have this environment activated.

To deactivate, simply::

    $ exit

Configuration
=============

The file `config.yaml` looks like::

    title: DATEX II v3.x vocab
    aliases:
        common: vocab/3/Common
        locref: vocab/3/LocationReferencing
        vms: vocab/3/Vms
        situation: vocab/3/Situation
        payload: vocab/3/D2Payload
        data: vocab/3/RoadTraficData

It contains list of paths to directories (incl. aliases) with `base.jsonld` files to process.

The `title` is used as title for generated `index.html`

Usage
=====
To convert JSON-LD file into alternative forms, put `base.jsonld` into directory configured in `config.yaml`

Then run (from project root) all the conversions at once::

    doit

This will do all sorts of conversions::

    $ tree vocab
    vocab
    └── 3
        ├── Common
        │   ├── base-compacted.jsonld
        │   ├── base-expanded.jsonld
        │   ├── base-flattened.jsonld
        │   ├── base.jsonld
        │   ├── base-normalized.nq
        │   ├── base.nt
        │   ├── base.n3
        │   ├── base.trig
        │   ├── base.ttl
        │   ├── base.xml
        │   └── index.html
        ├── D2Payload
        │   ├── base-compacted.jsonld
        │   ├── base-expanded.jsonld
        │   ├── base-flattened.jsonld
        │   ├── base.jsonld
        │   ├── base-normalized.nq
        │   ├── base.nt
        │   ├── base.n3
        │   ├── base.trig
        │   ├── base.ttl
        │   ├── base.xml
        │   └── index.html
        ├── LocationReferencing
        │   ├── base-compacted.jsonld
        │   ├── base-expanded.jsonld
        │   ├── base-flattened.jsonld
        │   ├── base.jsonld
        │   ├── base-normalized.nq
        │   ├── base.nt
        │   ├── base.n3
        │   ├── base.trig
        │   ├── base.ttl
        │   ├── base.xml
        │   └── index.html
        ├── RoadTraficData
        │   ├── base-compacted.jsonld
        │   ├── base-expanded.jsonld
        │   ├── base-flattened.jsonld
        │   ├── base.jsonld
        │   ├── base-normalized.nq
        │   ├── base.nt
        │   ├── base.n3
        │   ├── base.trig
        │   ├── base.ttl
        │   ├── base.xml
        │   └── index.html
        ├── Situation
        │   ├── base-compacted.jsonld
        │   ├── base-expanded.jsonld
        │   ├── base-flattened.jsonld
        │   ├── base.jsonld
        │   ├── base-normalized.nq
        │   ├── base.nt
        │   ├── base.n3
        │   ├── base.trig
        │   ├── base.ttl
        │   ├── base.xml
        │   └── index.html
        └── Vms
            ├── base-compacted.jsonld
            ├── base-expanded.jsonld
            ├── base-flattened.jsonld
            ├── base.jsonld
            ├── base-normalized.nq
            ├── base.nt
            ├── base.n3
            ├── base.trig
            ├── base.ttl
            ├── base.xml
            └── index.html

Open file `index.html` in project root to see index of files and particular vocabularies.

If you run command::

    $ doit publish

it will mirror all the files from `vocab` into `public` to make them visible as GitLab pages::

    $ tree public
    public
    ├── index.html
    └── vocab
        └── 3
            ├── Common
            │   ├── base-compacted.jsonld
            │   ├── base-expanded.jsonld
            │   ├── base-flattened.jsonld
            │   ├── base.jsonld
            │   ├── base-normalized.nq
            │   ├── base.nt
            │   ├── base.n3
            │   ├── base.trig
            │   ├── base.ttl
            │   ├── base.xml
            │   └── index.html
            ├── D2Payload
            │   ├── base-compacted.jsonld
            │   ├── base-expanded.jsonld
            │   ├── base-flattened.jsonld
            │   ├── base.jsonld
            │   ├── base-normalized.nq
            │   ├── base.nt
            │   ├── base.n3
            │   ├── base.trig
            │   ├── base.ttl
            │   ├── base.xml
            │   └── index.html
            ├── LocationReferencing
            │   ├── base-compacted.jsonld
            │   ├── base-expanded.jsonld
            │   ├── base-flattened.jsonld
            │   ├── base.jsonld
            │   ├── base-normalized.nq
            │   ├── base.nt
            │   ├── base.n3
            │   ├── base.trig
            │   ├── base.ttl
            │   ├── base.xml
            │   └── index.html
            ├── RoadTraficData
            │   ├── base-compacted.jsonld
            │   ├── base-expanded.jsonld
            │   ├── base-flattened.jsonld
            │   ├── base.jsonld
            │   ├── base-normalized.nq
            │   ├── base.nt
            │   ├── base.n3
            │   ├── base.trig
            │   ├── base.ttl
            │   ├── base.xml
            │   └── index.html
            ├── Situation
            │   ├── base-compacted.jsonld
            │   ├── base-expanded.jsonld
            │   ├── base-flattened.jsonld
            │   ├── base.jsonld
            │   ├── base-normalized.nq
            │   ├── base.nt
            │   ├── base.n3
            │   ├── base.trig
            │   ├── base.ttl
            │   ├── base.xml
            │   └── index.html
            └── Vms
                ├── base-compacted.jsonld
                ├── base-expanded.jsonld
                ├── base-flattened.jsonld
                ├── base.jsonld
                ├── base-normalized.nq
                ├── base.nt
                ├── base.n3
                ├── base.trig
                ├── base.ttl
                ├── base.xml
                └── index.html

Publishing to GitLab pages is done after new commits are pushed to `master` (see `.gitlab-ci.yml`)

GitLab page for our vocab are published on url: https://tamtamresearch-public.gitlab.io/rdf/jsonld2x/

Using `doit`
------------
Conversions are done using pydoit_

.. _pydoit: https://pydoit.org/tasks.html

To list all top level tasks::

    $ doit list
    common       
    data         
    locref       
    payload      
    publish      Publish to dir public
    root_index   Write root index.html
    situation    
    vms          

To list tasks incl. subtasks::

    $ doit list --all
    common                
    common:compact        base.jsonld->base-compacted.jsonld
    common:expand         base.jsonld->base-expanded.jsonld
    common:flatten        base.jsonld->base-flattened.jsonld
    common:index          Local index.html
    common:n3             base.jsonld->base.n3
    common:normalize      base.jsonld->base-normalize.nq
    common:nt             base.jsonld->base.nt
    common:trig           base.jsonld->base.trig
    common:turtle         base.jsonld->base.ttl
    common:xml            base.jsonld->base.xml
    data                  
    data:compact          base.jsonld->base-compacted.jsonld
    data:expand           base.jsonld->base-expanded.jsonld
    data:flatten          base.jsonld->base-flattened.jsonld
    data:index            Local index.html
    data:n3               base.jsonld->base.n3
    data:normalize        base.jsonld->base-normalize.nq
    data:nt               base.jsonld->base.nt
    data:trig             base.jsonld->base.trig
    data:turtle           base.jsonld->base.ttl
    data:xml              base.jsonld->base.xml
    locref                
    locref:compact        base.jsonld->base-compacted.jsonld
    locref:expand         base.jsonld->base-expanded.jsonld
    locref:flatten        base.jsonld->base-flattened.jsonld
    locref:index          Local index.html
    locref:n3             base.jsonld->base.n3
    locref:normalize      base.jsonld->base-normalize.nq
    locref:nt             base.jsonld->base.nt
    locref:trig           base.jsonld->base.trig
    locref:turtle         base.jsonld->base.ttl
    locref:xml            base.jsonld->base.xml
    payload               
    payload:compact       base.jsonld->base-compacted.jsonld
    payload:expand        base.jsonld->base-expanded.jsonld
    payload:flatten       base.jsonld->base-flattened.jsonld
    payload:index         Local index.html
    payload:n3            base.jsonld->base.n3
    payload:normalize     base.jsonld->base-normalize.nq
    payload:nt            base.jsonld->base.nt
    payload:trig          base.jsonld->base.trig
    payload:turtle        base.jsonld->base.ttl
    payload:xml           base.jsonld->base.xml
    publish               Publish to dir public
    root_index            Write root index.html
    situation             
    situation:compact     base.jsonld->base-compacted.jsonld
    situation:expand      base.jsonld->base-expanded.jsonld
    situation:flatten     base.jsonld->base-flattened.jsonld
    situation:index       Local index.html
    situation:n3          base.jsonld->base.n3
    situation:normalize   base.jsonld->base-normalize.nq
    situation:nt          base.jsonld->base.nt
    situation:trig        base.jsonld->base.trig
    situation:turtle      base.jsonld->base.ttl
    situation:xml         base.jsonld->base.xml
    vms                   
    vms:compact           base.jsonld->base-compacted.jsonld
    vms:expand            base.jsonld->base-expanded.jsonld
    vms:flatten           base.jsonld->base-flattened.jsonld
    vms:index             Local index.html
    vms:n3                base.jsonld->base.n3
    vms:normalize         base.jsonld->base-normalize.nq
    vms:nt                base.jsonld->base.nt
    vms:trig              base.jsonld->base.trig
    vms:turtle            base.jsonld->base.ttl
    vms:xml               base.jsonld->base.xml

To run conversions only for `situation`::

    $ doit situation
    .  situation:compact
    .  situation:xml
    .  situation:trig
    .  situation:n3
    .  situation:turtle
    .  situation:nt
    .  situation:expand
    .  situation:flatten
    .  situation:normalize
    .  situation:index

To remove files created in that task::

    $ doit clean situation
    situation:index - removing file 'vocab/3/Situation/index.html'
    situation:normalize - removing file 'vocab/3/Situation/base-normalized.nq'
    situation:flatten - removing file 'vocab/3/Situation/base-flattened.jsonld'
    situation:expand - removing file 'vocab/3/Situation/base-expanded.jsonld'
    situation:nt - removing file 'vocab/3/Situation/base.nt'
    situation:turtle - removing file 'vocab/3/Situation/base.ttl'
    situation:n3 - removing file 'vocab/3/Situation/base.n3'
    situation:trig - removing file 'vocab/3/Situation/base.trig'
    situation:xml - removing file 'vocab/3/Situation/base.xml'
    situation:compact - removing file 'vocab/3/Situation/base-compacted.jsonld'

To clean all files created by all tasks::

    $ doit clean
    data:index - removing file 'vocab/3/RoadTraficData/index.html'
    data:expand - removing file 'vocab/3/RoadTraficData/base-expanded.jsonld'
    data:trig - removing file 'vocab/3/RoadTraficData/base.trig'
    data:flatten - removing file 'vocab/3/RoadTraficData/base-flattened.jsonld'
    data:xml - removing file 'vocab/3/RoadTraficData/base.xml'
    data:n3 - removing file 'vocab/3/RoadTraficData/base.n3'
    data:nt - removing file 'vocab/3/RoadTraficData/base.nt'
    data:compact - removing file 'vocab/3/RoadTraficData/base-compacted.jsonld'
    data:turtle - removing file 'vocab/3/RoadTraficData/base.ttl'
    data:normalize - removing file 'vocab/3/RoadTraficData/base-normalized.nq'
    payload:index - removing file 'vocab/3/D2Payload/index.html'
    payload:flatten - removing file 'vocab/3/D2Payload/base-flattened.jsonld'
    payload:n3 - removing file 'vocab/3/D2Payload/base.n3'
    payload:xml - removing file 'vocab/3/D2Payload/base.xml'
    payload:nt - removing file 'vocab/3/D2Payload/base.nt'
    payload:turtle - removing file 'vocab/3/D2Payload/base.ttl'
    payload:compact - removing file 'vocab/3/D2Payload/base-compacted.jsonld'
    payload:expand - removing file 'vocab/3/D2Payload/base-expanded.jsonld'
    payload:normalize - removing file 'vocab/3/D2Payload/base-normalized.nq'
    payload:trig - removing file 'vocab/3/D2Payload/base.trig'
    vms:index - removing file 'vocab/3/Vms/index.html'
    vms:expand - removing file 'vocab/3/Vms/base-expanded.jsonld'
    vms:n3 - removing file 'vocab/3/Vms/base.n3'
    vms:nt - removing file 'vocab/3/Vms/base.nt'
    vms:xml - removing file 'vocab/3/Vms/base.xml'
    vms:trig - removing file 'vocab/3/Vms/base.trig'
    vms:normalize - removing file 'vocab/3/Vms/base-normalized.nq'
    vms:compact - removing file 'vocab/3/Vms/base-compacted.jsonld'
    vms:turtle - removing file 'vocab/3/Vms/base.ttl'
    vms:flatten - removing file 'vocab/3/Vms/base-flattened.jsonld'
    locref:index - removing file 'vocab/3/LocationReferencing/index.html'
    locref:n3 - removing file 'vocab/3/LocationReferencing/base.n3'
    locref:xml - removing file 'vocab/3/LocationReferencing/base.xml'
    locref:nt - removing file 'vocab/3/LocationReferencing/base.nt'
    locref:flatten - removing file 'vocab/3/LocationReferencing/base-flattened.jsonld'
    locref:compact - removing file 'vocab/3/LocationReferencing/base-compacted.jsonld'
    locref:turtle - removing file 'vocab/3/LocationReferencing/base.ttl'
    locref:normalize - removing file 'vocab/3/LocationReferencing/base-normalized.nq'
    locref:expand - removing file 'vocab/3/LocationReferencing/base-expanded.jsonld'
    locref:trig - removing file 'vocab/3/LocationReferencing/base.trig'
    common:index - removing file 'vocab/3/Common/index.html'
    common:compact - removing file 'vocab/3/Common/base-compacted.jsonld'
    common:trig - removing file 'vocab/3/Common/base.trig'
    common:flatten - removing file 'vocab/3/Common/base-flattened.jsonld'
    common:xml - removing file 'vocab/3/Common/base.xml'
    common:nt - removing file 'vocab/3/Common/base.nt'
    common:n3 - removing file 'vocab/3/Common/base.n3'
    common:turtle - removing file 'vocab/3/Common/base.ttl'
    common:expand - removing file 'vocab/3/Common/base-expanded.jsonld'
    common:normalize - removing file 'vocab/3/Common/base-normalized.nq'

To run only normalize conversion for locref::

    $ doit locref:normalize
    .  locref:normalize

Testing base.jsonld
===================

Use Google Structured Data Testing Tool
---------------------------------------
Go to https://search.google.com/structured-data/testing-tool/

Select "CODE SNIPPET" and paste into that complete JSON-LD file as is.

Click "RUN TEST" button.

It shall result in "Detected" report showing number of errors, warnings and items found.

Issues with the tool:

- if there is an error "unknown JSONLD value type", it does not locate the problem in the file.
- Google warns that this tools is being shut down (soon) and advise to use "Rich Results Test".


Use Google Rich Results Test
============================
To to https://search.google.com/test/rich-results

Select "CODE" and paste the code into there. ~the code must be embeded in HTML as follows::


    <html amp>
      <head>
        <title>Article headline</title>
        <script type="application/ld+json">
    {
      "@context": {
        ....
        ....
        ....
    }
        </script>
      </head>
      <body>
      </body>
    </html>

Click "RUN TEST" button and wait for the report.

This tool locates the issues much better.

When you are done, copy the tested JSON-LD document to this repo.

Use test suite from this repo
=============================
::

    pytest -sv tests

this will run tests suite on all DATEX II vocab vocabularies.

Note, that these tests are trying to detect issues discussed earlier.

Anyway, if a tests fails, it does not mean the ontology is necessarily bad. Take it as warning.
