from functools import cached_property
import json
from pathlib import Path
import shutil

import attr
from pyld import jsonld
from rdflib import Graph
import yaml


def load_cfg(fname="config.yaml"):
    path = Path(fname)
    assert path.exists()
    with path.open("r", encoding="utf-8") as f:
        return yaml.safe_load(f)


CFG = load_cfg("config.yaml")

DOIT_CONFIG = {"default_tasks": list(CFG["aliases"].keys())}


@attr.s(auto_attribs=True)
class XFormer:
    """Doc string for XFormer"""

    name = "abstract"
    source_name = "base.jsonld"
    parse_kwargs = {"format": "json-ld"}
    target_name = "some.target.file.jsonld"
    serialize_kwargs = {"format": "json-ld", "auto_compact": True, "sort_keys": True}

    wd: Path
    wd_alias: str

    @property
    def source(self):
        return self.wd / self.source_name

    @property
    def file_dep(self):
        return [self.source]

    @property
    def target(self):
        return self.source.with_name(self.target_name)

    @cached_property
    def source_json(self):
        with self.source.open("rb") as f:
            return json.load(f)

    def write_target_json(self, data):
        with self.target.open("w", encoding="utf-8") as f:
            json.dump(data, f, indent=2, sort_keys=True)

    def action(self):
        self.target.write_bytes(self.g.serialize(**self.serialize_kwargs))

    @cached_property
    def g(self):
        return Graph().parse(data=self.source.read_bytes(), **self.parse_kwargs)

    @property
    def plan(self):
        return {
            "basename": self.wd_alias,
            "name": self.name,
            "file_dep": self.file_dep,
            "actions": [(self.action)],
            "targets": [self.target],
            "clean": True,
            "doc": self.__doc__,
        }


@attr.s(auto_attribs=True)
class XFormerCompact(XFormer):
    """base.jsonld->base-compacted.jsonld"""

    name = "compact"
    target_name = "base-compacted.jsonld"
    serialize_kwargs = {"format": "json-ld", "auto_compact": True, "sort_keys": True}


@attr.s(auto_attribs=True)
class XFormerXml(XFormer):
    """base.jsonld->base.xml"""

    name = "xml"
    target_name = "base.xml"
    serialize_kwargs = {"format": "xml", "auto_compact": True, "sort_keys": True}


@attr.s(auto_attribs=True)
class XFormerTrig(XFormer):
    """base.jsonld->base.trig"""

    name = "trig"
    target_name = "base.trig"
    serialize_kwargs = {"format": "trig", "auto_compact": True, "sort_keys": True}


@attr.s(auto_attribs=True)
class XFormerN3(XFormer):
    """base.jsonld->base.n3"""

    name = "n3"
    target_name = "base.n3"
    serialize_kwargs = {"format": "n3", "auto_compact": True, "sort_keys": True}


@attr.s(auto_attribs=True)
class XFormerTurtle(XFormer):
    """base.jsonld->base.ttl"""

    name = "turtle"
    target_name = "base.ttl"
    serialize_kwargs = {"format": "turtle", "auto_compact": True, "sort_keys": True}


@attr.s(auto_attribs=True)
class XFormerNt(XFormer):
    """base.jsonld->base.nt"""

    name = "nt"
    target_name = "base.nt"
    serialize_kwargs = {"format": "nt", "auto_compact": True, "sort_keys": True}


@attr.s(auto_attribs=True)
class XFormerExpand(XFormer):
    """base.jsonld->base-expanded.jsonld"""

    name = "expand"
    target_name = "base-expanded.jsonld"

    def action(self):
        res = jsonld.expand(self.source_json)
        self.write_target_json(res)


@attr.s(auto_attribs=True)
class XFormerFlatten(XFormer):
    """base.jsonld->base-flattened.jsonld"""

    name = "flatten"
    target_name = "base-flattened.jsonld"

    def action(self):
        res = jsonld.flatten(self.source_json)
        self.write_target_json(res)


@attr.s(auto_attribs=True)
class XFormerNormalize(XFormer):
    """base.jsonld->base-normalize.nq"""

    name = "normalize"
    target_name = "base-normalized.nq"

    def action(self):
        res = jsonld.normalize(
            self.source_json,
            {"algorithm": "URDNA2015", "format": "application/n-quads"},
        )
        self.target.write_text(res, encoding="utf-8")


X_CLASS_RDF_LST = [
    XFormerTurtle,
    XFormerXml,
    XFormerN3,
    XFormerNt,
    XFormerTrig,
    XFormerCompact,
    XFormerExpand,
    XFormerFlatten,
    XFormerNormalize,
]


@attr.s(auto_attribs=True)
class XFormerIndex(XFormer):
    """Local index.html"""

    name = "index"
    target_name = "index.html"
    x_lst = X_CLASS_RDF_LST

    @property
    def file_dep(self):
        return [self.source] + [self.wd / x.target_name for x in self.x_lst]

    def action(self):
        with self.target.open("w", encoding="utf-8") as f:
            f.write(f"<html><body><h1>{self.wd.name}</h1><ul>")
            for pth in self.file_dep:
                f.write(f"<li><a href='{pth.name}'>{pth.name}</a></li>")
            f.write(f"</ul></body></html>")


X_CLASS_IDX_LST = [XFormerIndex]
X_CLASS_ALL = X_CLASS_RDF_LST + X_CLASS_IDX_LST


def task_all():
    for diralias, dirpath in CFG["aliases"].items():
        dirpath = Path(dirpath)
        for xform_class in X_CLASS_ALL:
            xformer = xform_class(dirpath, diralias)
            yield xformer.plan


def task_root_index():
    """Write root index.html"""
    file_dep = [Path(dirpath) / "index.html" for dirpath in CFG["aliases"].values()]
    target = Path("index.html")

    def write_index():
        with target.open("w", encoding="utf-8") as f:
            f.write(f"<html><body><h1>{CFG['title']}</h1><ul>")
            for pth in file_dep:
                f.write(f"<li><a href='{pth}'>{pth.parent}</a></li>")
            f.write(f"</ul></body></html>")

    return {
        "actions": [(write_index)],
        "file_dep": file_dep,
        "targets": [target],
        "clean": True,
    }


def task_publish():
    """Publish to dir public"""
    target_dir = Path("public")
    root_index = Path("index.html")

    names = [XFormer.source_name] + [x.target_name for x in X_CLASS_ALL]

    def deps_targets():
        file_dep = []
        targets = []
        for subdir in CFG["aliases"].values():
            src_subdir = Path(subdir)
            target_subdir = target_dir / subdir
            for name in names:
                file_dep.append(src_subdir / name)
                targets.append(target_subdir / name)
        file_dep.append(root_index)
        targets.append(target_dir / root_index.name)
        return file_dep, targets

    def publish():
        target_dir.mkdir(exist_ok=True)
        shutil.copy(root_index, target_dir)
        for subdir in CFG["aliases"].values():
            src_subdir = Path(subdir)
            target_subdir = target_dir / subdir
            target_subdir.mkdir(parents=True, exist_ok=True)
            for name in names:
                shutil.copy(src_subdir / name, target_subdir / name)

    file_dep, targets = deps_targets()
    return {
        "file_dep": file_dep,
        "actions": [(publish)],
        "targets": targets,
        "clean": True,
    }
