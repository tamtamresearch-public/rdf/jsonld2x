from pathlib import Path

import pytest
import rdflib


@pytest.fixture(
    scope="session",
    params=[
        "Common",
        "D2Payload",
        "LocationReferencing",
        "RoadTraficData",
        "Situation",
        "Vms",
    ],
)
def vocab_fname(request):
    vocab_dir = Path("vocab/3")
    path = vocab_dir / request.param / "base.jsonld"
    assert path.exists()
    return path


@pytest.fixture(scope="session")
def graph(vocab_fname):
    formats = {".nq": "nquads", ".jsonld": "json-ld"}
    try:
        format = formats[vocab_fname.suffix]
    except KeyError:
        pytest.fail(f"Did not find format for extension {vocab_fname.suffix}")
    return rdflib.Graph().parse(str(vocab_fname), format=format)


def test_double_data_object_property(graph):
    """
    Protege complains, where single subject declares rdf:type
    for both owl:ObjectProperty and owl:DatatypeProperty

    Prints those subjects
    """

    qres = graph.query(
        """
        PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        SELECT ?subject 
        WHERE 
          { ?subject rdf:type owl:ObjectProperty, owl:DatatypeProperty} 
            """
    )

    lst = qres.bindings
    if lst:
        subj_lst = [rec["subject"].n3() for rec in lst]
        # list of subjects with both owl:ObjectProperty and owl:DatatypeProperty
        print(subj_lst)
        assert (
            not subj_lst
        ), "list of subjects with owl:{Object,Datatype}Property is empty"


def test_unresolved_prefix(graph):
    """Detect object literals resembling unresolved IRI"""

    qres = graph.query(
        """
        SELECT ?s ?p ?o
        WHERE 
          { ?s ?p ?o
          FILTER isLiteral(?o)
          FILTER regex (?o, "^[A-Za-z][A-Za-z0-9-_]*:[A-Za-z0-9-_]*$")
          } 
            """
    )

    lst = qres.bindings
    if lst:
        trip_lst = [f"{rec['s'].n3()} {rec['p'].n3()} {rec['o'].n3()}." for rec in lst]
        # list of subjects with both owl:ObjectProperty and owl:DatatypeProperty
        for rec in trip_lst:
            print(rec)
        assert not trip_lst, "no object literals resembling unresolved qname"
